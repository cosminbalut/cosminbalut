package week12;

import week12.domain.Accommodation;
import week12.domain.AccommodationRoomFairRelation;
import week12.domain.RoomFair;

public class Main {

    public static void main(String args[]) {

        Accommodation accommodation = new Accommodation();
        accommodation.start();

        RoomFair roomFair = new RoomFair();
        roomFair.start();

       AccommodationRoomFairRelation accommodationFairRelation = new AccommodationRoomFairRelation();
        accommodationFairRelation.start();
    }
}