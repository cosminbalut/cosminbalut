package week12.domain;

import java.sql.*;

public class Accommodation extends Thread {

        @Override
        public void run() {

                try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework", "postgres", "Database123")) {

                        PreparedStatement preparedStatement1 = connection.prepareStatement("CREATE TABLE if not exists accommodation" +
                                "(ID int NOT NULL PRIMARY KEY, Type varchar(32), Bed_type varchar(32), Max_guests int, Description varchar(512), unique(ID))");

                        preparedStatement1.executeUpdate();

                        for (int i = 1; i <= 4; i++) {
                                String s = "insert into Accommodation values(";
                                s += i + "," + "'Premium Double-Room'"
                                        + "," + "'California King Size'"
                                        + "," + 2
                                        + "," + "'this room has a beautiful view, and very large')";

                                PreparedStatement st = preparedStatement1.getConnection().prepareStatement(s);
                                st.executeUpdate();

                        }
                }

                catch (SQLException e){
                        e.printStackTrace();
                }
        }

}


