package week12.domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

public class RoomFair extends Thread{
    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework",
                "postgres", "Database123")) {

            PreparedStatement preparedStatement2 = connection.prepareStatement("CREATE TABLE if not exists room_fair "
                    + "(ID int NOT NULL PRIMARY KEY, Value double precision, Season varchar(32))");

            preparedStatement2.executeUpdate();

            for (int i = 1; i <= 4; i++) {
                String s = "insert into room_fair values(";
                s += i + "," + 350.34
                        + "," + "'Summer')";
                PreparedStatement st = preparedStatement2.getConnection().prepareStatement(s);
                st.executeUpdate();

            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
