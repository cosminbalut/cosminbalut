package week9;

import week9.domain.Gender;
import week9.domain.Student;
import week9.domain.StudentRepository;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        StudentRepository studentRepository = new StudentRepository();

        Student student1 = new Student("Cosmin","Băluț", LocalDate.of(1996,11,6), Gender.M,"1234567890123");
        Student student2 = new Student("Costel","Alexandru", LocalDate.of(1997,1,26),Gender.M,"1234577820123");
        Student student3 = new Student("Alina","Matei", LocalDate.of(1984,12,16),Gender.F,"2345679890091");
        Student student4 = new Student("Daniela","Florea", LocalDate.of(2000,4,22),Gender.F,"1234577820123");

        studentRepository.addStudent(student1);
        studentRepository.addStudent(student2);
        studentRepository.addStudent(student3);
        studentRepository.addStudent(student4);

        studentRepository.listStudents();

        studentRepository.deleteUsingCNP("1234577820123");
        studentRepository.listStudents();

        System.out.println(studentRepository.retrieveStudents("26"));
        System.out.println(studentRepository.retrieveStudents("22"));

    }
}
