package week9.domain;
import java.time.LocalDate;

public class Student {

    private final String firstName,lastName;
    private final LocalDate dateOfBirth;
    private final Gender gender;
    private String CNP;

    public Student(String  firstName, String lastName, LocalDate dateOfBirth, Gender gender, String CNP) {

        if (firstName != null){
            this.firstName = firstName;
        }else{
            throw new MyException("First name should not be empty!");
        }
        if (lastName != null){
            this.lastName = lastName;
        }else{
            throw  new MyException("Last name should not be empty!");
        }


        this.gender = gender;

        int birthYear = dateOfBirth.getYear();
        if (birthYear < 1900){
            throw new MyException("Error! You are too old! Are you sure you're still alive?");

        }else if (birthYear > LocalDate.now().getYear() - 18){
            throw new MyException("The minimum age is 18 years. thanks for understanding!");
        }else{
            this.dateOfBirth = dateOfBirth;
        }

        if (CNP != null ){
            this.CNP = CNP;
        } else {
            throw new MyException("Please fill in the blanks, then try again");
        }
        if (CNP.length() == 13) {
            this.CNP = CNP;
        }else{
            throw new MyException("Wrong CNP. Please try again");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public String getCNP() {
        return CNP;
    }


    @Override
    public String toString() {

        return "Name: " + firstName + " " + lastName + "\n"+
                "Date of birth: " + dateOfBirth + "\n"+
                "Gender: " + gender + "\n"+
                "CNP: " + CNP;
    }

}
