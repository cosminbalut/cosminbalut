package week9.domain;

public class MyException extends RuntimeException {
    public MyException(String message) {
        super( message);
    }
}
