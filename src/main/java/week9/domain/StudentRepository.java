package week9.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StudentRepository {

    public List<Student> students = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(StudentRepository.class);


    public void addStudent(Student student) {
        this.students.add(student);
    }

    public void deleteUsingCNP(String CNP) {

        if (CNP == null) {
            logger.error("CNP is empty");
            throw new MyException("Please fill in the blanks!");
        }
        if (CNP.length() != 13 ){
            logger.error("CNP is wrong");
            throw new MyException("Please add a valid cnp!");
        }

        boolean found = false;
        for (Student student : students) {
            if (student.getCNP().equals(CNP)) {
                students.remove(student);
                found = true;
                break;
            }
        }

        if (!found) {
            logger.error("This student does not exist!");
            throw new MyException("This CNP does not exist");
        }
    }


    public List<Student> retrieveStudents(String age) {

        System.out.println("Display the student by age!!!");
        System.out.println();

        logger.trace("Retrieving students");

        int value;
        try {
            value = Integer.parseInt(age);
        } catch (NumberFormatException exception) {
            throw new MyException("Age isn't a number!");
        }

        if (value < 0) {
            throw new MyException("Age is negative!");
        }

        List<Student> studentsByAge = new ArrayList<>();

        int calculatedAge;
        for (Student student : students) {
            calculatedAge = LocalDate.now().getYear() - student.getDateOfBirth().getYear();
            if (calculatedAge == value) {
                studentsByAge.add(student);
            }

        }
        return studentsByAge;
    }

    public void listStudents() {

        System.out.println("Display before and after sorting!!!");
        System.out.println();

        for (Student student : students) {

            System.out.println("Name: " + student.getFirstName() + " " + student.getLastName() + "\n"
                    + "Date of birth: " + student.getDateOfBirth() + "\n"
                    + "Gender: " + student.getGender() + "\n"
                    + "CNP: " + student.getCNP());
            System.out.println();
        }
        this.students.sort(Comparator.comparingInt(o -> o.getDateOfBirth().getYear()));

    }
}
