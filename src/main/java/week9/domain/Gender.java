package week9.domain;

public enum Gender {
    M ("Male"),
    F ("Female");

    private String gender;

    Gender(String gender) {
        this.gender = gender;
    }

        public static Gender forValue (String s){
        for (Gender g: values()){
            if (g.getGender().equalsIgnoreCase(s)){
                return g;
            }
        }
        throw new RuntimeException("Not a valid gender");
    }


    private String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return gender;
    }
}
