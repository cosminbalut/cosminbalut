package week5;

import java.util.Objects;

public class Person implements Comparable{
        protected String name;
        protected int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Person() {

        }


    public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
        public int compareTo(Object o) {
            Person person = (Person) o;
            return this.name.compareTo(person.name);             //Sorted by name
//
//            if (this.age < person.age){                        //Sorted by age
//                return -1;
//            }else if (this.age == person.age){
//                return 0;
//
//            }else{
//                return 1;
//            }
        }



    @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

    }

