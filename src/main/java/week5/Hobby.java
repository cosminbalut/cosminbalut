package week5;

import java.util.ArrayList;
import java.util.List;

public class Hobby {

    private String nameOfTheHobby;
    private int frequency;
    private List<Address> addresses = new ArrayList<>();

    public Hobby(String nameOfTheHobby, int frequency, List<Address> addresses) {
        this.nameOfTheHobby = nameOfTheHobby;
        this.frequency = frequency;
        this.addresses = addresses;
    }

    public String getNameOfTheHobby() {
        return nameOfTheHobby;
    }

    public void setNameOfTheHobby(String nameOfTheHobby) {
        this.nameOfTheHobby = nameOfTheHobby;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "nameOfTheHobby='" + nameOfTheHobby + '\'' +
                ", frequency=" + frequency +
                ", addresses=" + addresses +
                '}';
    }

}
