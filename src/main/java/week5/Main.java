package week5;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        ////////////////////////////////////////////////// Create a persons list

        Person person1 = new Unemployed("Daniela", 28);
        Person person2 = new Unemployed("Alina", 24);
        Person person3 = new Student("Sorin", 26);
        Person person4 = new Student("Edi", 21);
        Person person5 = new Hired("Alexa", 35);
        Person person6 = new Hired("Florin", 26);

        ////////////////////////////////////////////////// Created and added persons to TreeSet

        Set<Person> sortedPersons = new TreeSet<>();
        sortedPersons.add(person1);
        sortedPersons.add(person2);
        sortedPersons.add(person3);
        sortedPersons.add(person4);
        sortedPersons.add(person5);
        sortedPersons.add(person6);

//        for (Person persons : sortedPersons) {
//            System.out.println("Name: " + persons.getName() + "," + " Age: " + persons.getAge());
//        }

        for (Person i : sortedPersons) {
            System.out.println("Name: " + i.getName() + " Age: " + i.getAge());
        }

        //////////////////////////////////////////////////  Addresses were created

        Address address1 = new Address("Elvetia", "Basel", "St. Jakob Park", 88);
        Address address2 = new Address("Romania", "Brasov", "Str. Ioan Popasu", 35);
        Address address3 = new Address("Romania", "Cluj", "Str. Lalelelor", 3);
        Address address4 = new Address("Franta", "Paris", "Str.  Av. de Suffren", 90);
        Address address5 = new Address("Spania", "Madrid", "St. P.º del Prado", 8);
        Address address6 = new Address("Spania", "Barcelona", "Str. Pla de Palau", 22);

        ////////////////////////////////////////////////// Addresses list for hobbies were created and added

        List<Address> addressForSwimming = new ArrayList<>();
        addressForSwimming.add(address1);
        addressForSwimming.add(address4);

        List<Address> addressForFootball = new ArrayList<>();
        addressForFootball.add(address2);
        addressForFootball.add(address3);

        List<Address> addressForCarRacing = new ArrayList<>();
        addressForCarRacing.add(address5);
        addressForCarRacing.add(address6);

        List<Address> addressForBallet = new ArrayList<>();
        addressForBallet.add(address3);
        addressForBallet.add(address6);

        ////////////////////////////////////////////////// Create hobbies

        Hobby hobby1 = new Hobby("Football", 2, addressForFootball);
        Hobby hobby2 = new Hobby("Car racing", 1, addressForCarRacing);
        Hobby hobby3 = new Hobby("Swimming", 3, addressForSwimming);
        Hobby hobby4 = new Hobby("Ballet", 4, addressForBallet);

        List<Hobby> hobbiesForLadies = new ArrayList<>();
        hobbiesForLadies.add(hobby4);
        hobbiesForLadies.add(hobby3);

        List<Hobby> hobbiesForMen = new ArrayList<>();
        hobbiesForMen.add(hobby1);
        hobbiesForMen.add(hobby2);

        HashMap<Person, List<Hobby>> personsAndHobbies = new HashMap<>();
        personsAndHobbies.put(person1, hobbiesForLadies);
        personsAndHobbies.put(person2, hobbiesForLadies);
        personsAndHobbies.put(person3, hobbiesForMen);
        personsAndHobbies.put(person4, hobbiesForMen);
        personsAndHobbies.put(person5, hobbiesForLadies);
        personsAndHobbies.put(person6, hobbiesForMen);

        System.out.println();

        System.out.println("Name: " + person1.getName()+  " , " + "Age: " + person1.getAge());
        for (Hobby hobby : personsAndHobbies.get(person1)) {
            System.out.print( "Hobby: " + hobby.getNameOfTheHobby() + " / " + hobby.getFrequency()+ " times a week " + " ; ");
        }

        System.out.println();
        System.out.println("////////////////////////////////////////////////////////////////////");

        System.out.println("Name: " + person4.getName()+  " , " + "Age: " + person4.getAge());
        for (Hobby hobby : personsAndHobbies.get(person4)) {
            System.out.print( "Hobby: " + hobby.getNameOfTheHobby() + " / " + hobby.getFrequency()+ " times a week " + " ; ");
        }

        System.out.println();
        System.out.println("////////////////////////////////////////////////////////////////////");

        System.out.println("Name: " + person3.getName()+  " , " + "Age: " + person3.getAge());
        for (Hobby h : personsAndHobbies.get(person3)) {
            System.out.print( "Hobby: " + h.getNameOfTheHobby() + " / " + h.getFrequency()+ " times a week " + " ; ");
            for (Address a : h.getAddresses());
            System.out.println();
            System.out.println("Address: " + h.getAddresses());
        }




    }
    }





