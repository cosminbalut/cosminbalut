package week5;

public class Hired extends Person {


    public Hired(String name, int age) {
        super(name, age);
    }


    @Override
    public String toString() {
        return "Hired{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}