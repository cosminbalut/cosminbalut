package week10.domain;

import java.util.Comparator;

public class PersonComparator implements Comparator<PersonList> {

    @Override
    public int compare(PersonList o1, PersonList o2) {
        String name1 = o1.getLastName(), name2 =  o2.getLastName();
        if (name1.equals(name2)){
            return 0;
        }
        return name1.compareTo(name2);
        }
}
