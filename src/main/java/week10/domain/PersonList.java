package week10.domain;
import java.time.LocalDate;
import java.util.TreeSet;

public class PersonList {
    private String firstName, lastName;
    private LocalDate dateOfBirth;


    public PersonList(String firstName, String lastName, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = LocalDate.parse(dateOfBirth);
    }

    public static TreeSet<PersonList> nameOfFunction(String month, TreeSet<PersonList> personLists){
        TreeSet<PersonList> personWithoutMonthY = new TreeSet<>(new PersonComparator());

        System.out.println("\n" +"Sorting by date!!");
        for (PersonList p : personLists){

            if (p.getDateOfBirth().getMonth().toString().equals(month)){
                personWithoutMonthY.add(p);

            }
        }
        return personWithoutMonthY;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {

        return  "\n " + "First Name: " + firstName +
                "\n " + "Last Name: " + lastName +
                "\n " ;
    }
}
