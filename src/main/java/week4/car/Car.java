package week4.car;

import week4.vehicle.Vehicle;

    public abstract class Car implements Vehicle {
        protected double averageFuelConsumption;
        private String name;
        private String model;
        private int fuelTankSize;
        private String fuelType;
        private String chassisNumber;
        public int maxGears;
        public double fuelConsumedPer100Km;
        private int tireSize;
        public double fuelConsumptionForShiftGear;
        public double consumption;
        protected double availableFuel;

        public Car() {

        }

        private String getCar() {
            return name + model;
        }

        public Car(String chassisNumber, int tireSize, double availableFuel) {
            this.chassisNumber = chassisNumber;
            this.tireSize = tireSize;
            this.availableFuel = availableFuel;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }


        public void setFuelTankSize(int fuelTankSize) {
            this.fuelTankSize = fuelTankSize;
        }


        public void setFuelType(String fuelType) {
            this.fuelType = fuelType;
        }


        public void setFuelConsumptionForShiftGear(double fuelConsumptionForShiftGear) {
            this.fuelConsumptionForShiftGear = fuelConsumptionForShiftGear;
        }

        @Override
        public void start() {
            System.out.println("The car " + getCar() + " comes out of the parking");
            System.out.println("Available fuel is: " + availableFuel + " liters");
        }

        @Override
        public void availableFuel() {

            System.out.println("The available fuel after the car has stopped is " + availableFuel + " liters");
        }

        public double getAvailableFuel() {
            return availableFuel;
        }

        public int setAvailableFuel(double availableFuel) {
            this.availableFuel = availableFuel;
            return 0;
        }

        @Override
        public void stop() {
            System.out.println("The car is parked");
        }


        public void setFuelConsumedPer100Km(double fuelConsumedPer100Km) {
            this.fuelConsumedPer100Km = fuelConsumedPer100Km;
        }



        @Override
        public String toString() {
            return "Car{" +
                    "name='" + name + '\'' +
                    ", model='" + model + '\'' +
                    ", fuelTankSize=" + fuelTankSize +
                    ", fuelType='" + fuelType + '\'' +
                    ", chassisNumber='" + chassisNumber + '\'' +
                    ", maxGears=" + maxGears +
                    ", fuelConsumedPer100Km=" + fuelConsumedPer100Km +
                    ", tireSize=" + tireSize +
                    ", fuelConsumptionForShiftGear=" + fuelConsumptionForShiftGear +
                    ", consumption=" + consumption +
                    ", availableFuel=" + availableFuel +
                    ", averageFuelConsumption=" + averageFuelConsumption +
                    '}';
        }
    }




