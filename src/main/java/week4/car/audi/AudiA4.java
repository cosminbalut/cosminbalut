package week4.car.audi;

public class AudiA4 extends Audi {



    private final double fuelConsumptionForShiftGear;
    private final double consumption;
    private double consumptionPerTotal;
    private double averageFuelConsumption;

    public AudiA4(String chassisNumber, int tireSize, double availableFuel) {
        super(chassisNumber, tireSize, availableFuel);
        this.setModel("A4 Quattro");
        this.maxGears = 6;
        this.setFuelType("Diesel");
        this.fuelConsumptionForShiftGear =0.2;
        this.consumption = 6.1;

    }


    @Override
    public void setFuelConsumptionForShiftGear(double fuelConsumptionForShiftGear) {
        super.setFuelConsumptionForShiftGear(fuelConsumptionForShiftGear);

    }



    @Override
    public void start() {
        super.start();
    }

    @Override
    public void shiftGear(int gear) {
        if (gear > maxGears) {
            System.out.println("The car has a limit of " + maxGears + " gears");
        } else {
            System.out.println("The car " + getName() + getModel() + "shifted to gear:" + gear);
            double consumptionPerGear = gear * fuelConsumptionForShiftGear;
            System.out.println("For each shift gear, the consumption increased with " + consumptionPerGear + " %");
            this.consumptionPerTotal = consumption + consumptionPerGear;
        }
    }




    @Override
    public double drive(double distance) {
        if (availableFuel == 0) {
            System.out.println("Fuel tank empty!!");
        } else {
            System.out.println("  Drives " + distance + " Km " + "with consumption of " + consumptionPerTotal  + " %");
        }
        double fuelUsed = ((distance * consumption) / 100);
        System.out.println("The car consumption for first gear is: " + fuelUsed + " liters");
        this.availableFuel = availableFuel - fuelUsed;
        return availableFuel;
    }

    @Override
    public void averageFuelConsumption() {
        if (consumption > 10) {
            System.out.println("The  car consumes too much ");
        } else {
            double averageFuelConsumption;
            averageFuelConsumption =  consumptionPerTotal;
            System.out.println("The average fuel consumption is " + averageFuelConsumption + " %");
        }
    }


    @Override
    public void stop() {
        super.stop();
    }
    @Override
    public void availableFuel() {

        System.out.println("The available fuel after the car has stopped is " + availableFuel + " liters");
    }




}
