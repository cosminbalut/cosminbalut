package week4.car.audi;

import week4.car.Car;

public abstract class Audi extends Car {
    public Audi(String chassisNumber, int tireSize, double availableFuel) {
        super(chassisNumber, tireSize, availableFuel);
        this.setName("Audi ");
    }


    @Override
    public void shiftGear(int gear) {

    }



}
