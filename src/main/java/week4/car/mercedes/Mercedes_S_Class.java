package week4.car.mercedes;

public class Mercedes_S_Class extends Mercedes {
    public Mercedes_S_Class(String name, int fuelTankSize, String fuelType, int gear, String chassisNumber, double consumptionPer100Km, double availableFuel, int tireSize, double fuelConsumptionForShiftGear) {
        super(name, fuelTankSize, fuelType, gear, chassisNumber, consumptionPer100Km, availableFuel, tireSize, fuelConsumptionForShiftGear);
    }
}
