package week4.car.mercedes;

import week4.car.Car;

public class Mercedes extends Car {

    public Mercedes(String chassisNumber, int tireSize, double availableFuel) {
        super(chassisNumber, tireSize, availableFuel);
        this.setName("Mercedes ");
    }

    public Mercedes(String name, int fuelTankSize, String fuelType, int gear, String chassisNumber, double consumptionPer100Km, double availableFuel, int tireSize, double fuelConsumptionForShiftGear) {
    }


    @Override
    public void shiftGear(int gear) {

    }

    @Override
    public double drive(double distance) {
        return 0;
    }

    @Override
    public void availableFuel() {

    }

    @Override
    public void averageFuelConsumption() {

    }
}