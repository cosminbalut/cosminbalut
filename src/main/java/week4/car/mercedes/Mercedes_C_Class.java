package week4.car.mercedes;

public class Mercedes_C_Class extends Mercedes {



    public Mercedes_C_Class(String chassisNumber, int tireSize, double availableFuel) {
        super(chassisNumber, tireSize, availableFuel);
        this.setModel("C400 4Matic 7G-TRONIC");
        this.maxGears = 6;
        this.setFuelType("Diesel");
        this.fuelConsumedPer100Km = getFuelConsumedPer100Km();
        this.setFuelConsumptionForShiftGear(0.7);
        this.consumption = 9.3;
        this.setFuelTankSize(60);
    }

    private double getFuelConsumedPer100Km() {
        return 0;
    }


}