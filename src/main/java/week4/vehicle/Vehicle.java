package week4.vehicle;

public interface Vehicle {

    void start();
    void stop();
    void shiftGear(int gear);
    double drive(double distance);
    void availableFuel();
    void averageFuelConsumption();


}