package week4;

import week4.car.Car;
import week4.car.audi.AudiA4;
import week4.car.audi.AudiA5;
import week4.car.mercedes.Mercedes_C_Class;
import week4.vehicle.Vehicle;

public class Main {
    public static void main(String[] args) {


        Car car = new AudiA4("RO5446NNN345WT", 34, 50);

        car.start();
        System.out.println();
        car.shiftGear(1);
        car.drive(0.01);
        System.out.println();
        car.shiftGear(2);
        car.drive(0.02);
        System.out.println();
        car.shiftGear(3);
        car.drive(0.05);
        System.out.println();
        car.shiftGear(4);
        car.drive(0.1);
        System.out.println();
        car.shiftGear(5);
        car.drive(10);
        System.out.println();
        car.shiftGear(6);
        car.drive(4);
        System.out.println();
        car.shiftGear(7);// Masina are 6 trepte de viteza, am afisat si mesaj de eroare pentru un numar mai mare!
        System.out.println();
        car.shiftGear(5);
        car.drive(10);
        System.out.println();
        car.shiftGear(4);
        car.drive(0.1);
        System.out.println();
        car.shiftGear(3);
        car.drive(0.05);
        System.out.println();
        car.shiftGear(2);
        car.drive(0.01);
        System.out.println();
        car.stop();
        car.availableFuel();
        car.averageFuelConsumption();



        System.out.println();

        Vehicle vehicle = new AudiA5("RO32543NV54B", 18, 40);
        vehicle.start();
        vehicle.shiftGear(1);
        vehicle.drive(1);
        vehicle.stop();
        vehicle.availableFuel();
        vehicle.averageFuelConsumption();


        System.out.println();

        Car car1 = new Mercedes_C_Class("943MFKDF8YNFF", 19, 60);

        car1.setFuelConsumedPer100Km(6.4);
        System.out.println(car1);

        car1.setAvailableFuel(44);
        System.out.println(car1);

    }
}