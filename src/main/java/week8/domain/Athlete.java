package week8.domain;

public class Athlete {

    public enum CountryCode {
        UK, RO, CZ, USA, SK
    }


    private int athleteNumber;
    private String athleteName,skiTimeResult, firstShootingRange, secondShootingRange, thirdShootingRange;
    private CountryCode countryCode;
    private int penaltyTime;
    private int addTime;
    private int timeBeforePenalty, timeAfterPenalty;


    public Athlete(int athleteNumber, String athleteName, String skiTimeResult, String countryCode, String penaltyTime) {
        this.athleteNumber = athleteNumber;
        this.athleteName = athleteName;
        this.skiTimeResult = skiTimeResult;
        this.countryCode = CountryCode.valueOf(countryCode);
        this.timeBeforePenalty = toSecond(skiTimeResult);
        this.penaltyTime = getAddPenalty(penaltyTime);
        this.timeAfterPenalty = timeBeforePenalty + this.penaltyTime;

    }

    public String convertTime(){
        int time = timeAfterPenalty;
        int minutes = time/60;
        int seconds = time - minutes * 60;
        return Integer.toString(minutes) + ":" + Integer.toString(seconds);
    }

    private static int getAddPenalty(String penaltyTime){
        int totalTime = 0;
        for (int i = 0;i<penaltyTime.length();i++){
            if (penaltyTime.charAt(i) == 'o')
                totalTime +=10;
        }
        return totalTime;
    }

    public Athlete() {

    }


    static int toSecond(String s) {
        String[] minSec = s.split(":");
        int minutes = Integer.parseInt(minSec[0]);
        int seconds = Integer.parseInt(minSec[1]);
        int minutesInSeconds = minutes * 60;
        return minutesInSeconds + seconds;
    }




    public int getAthleteNumber() {
        return athleteNumber;
    }

    public void setAthleteNumber(int athleteNumber) {
        this.athleteNumber = athleteNumber;
    }

    public String getAthleteName() {
        return athleteName;
    }

    public void setAthleteName(String athleteName) {
        this.athleteName = athleteName;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public String getSkiTimeResult() {
        return skiTimeResult;
    }

    public int getTimeAfterPenalty(){
        return timeAfterPenalty;
    }

    public void setSkiTimeResult(String skiTimeResult) {
        this.skiTimeResult = skiTimeResult;
    }

    public String getFirstShootingRange() {
        return firstShootingRange;
    }

    public void setFirstShootingRange(String firstShootingRange) {
        this.firstShootingRange = firstShootingRange;
    }

    public String getSecondShootingRange() {
        return secondShootingRange;
    }

    public void setSecondShootingRange(String secondShootingRange) {
        this.secondShootingRange = secondShootingRange;
    }

    public String getThirdShootingRange() {
        return thirdShootingRange;
    }

    public void setThirdShootingRange(String thirdShootingRange) {
        this.thirdShootingRange = thirdShootingRange;
    }

    public int getPenaltyTime() {
        return penaltyTime;
    }

    public void setPenaltyTime(int penaltyTime) {
        this.penaltyTime = penaltyTime;
    }

    public int getAddTime() {
        return addTime;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "Person{" +
                "athleteNumber=" + athleteNumber +
                ", athleteName='" + athleteName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", skiTimeResult='" + skiTimeResult + '\'' +
                ", timeAfterPenalty=" + this.convertTime() +
                ", penaltyTime=" + penaltyTime +
                '}';
    }
}

