package week8.domain;

import week8.domain.Athlete;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

    public int compare(Athlete o1, Athlete o2) {

        int timeA = o1.getTimeAfterPenalty(), timeB = o2.getTimeAfterPenalty() ;
            if (timeA != timeB) {
                if (timeA > timeB)
                    return 1;
                return -1;
            }
            return 0;
        }
    }

