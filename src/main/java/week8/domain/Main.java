package week8.domain;

import week8.domain.Athlete;
import week8.domain.AthleteComparator;

import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        TreeSet<Athlete> athletes = new TreeSet<>(new AthleteComparator());


        File filePath = new File("C:\\Users\\Ashima\\IdeaProjects\\cosminbalut\\src\\main\\java\\week8\\athlete.csv");

        BufferedReader reader = null;
        System.out.println("List of ski biathlon participants: " );
        System.out.println();

        try {
            String line = "";
            reader = new BufferedReader(new FileReader(filePath));
            reader.readLine();

            while ((line = reader.readLine()) != null) {

                String[] fields = line.split(",");
                Athlete athlete = new Athlete(Integer.parseInt(fields[0]),fields[1],fields[3],fields[2],fields[4]+fields[5]+fields[6]);
                athletes.add(athlete);
                System.out.println(line);
            }

            System.out.println();
            JOptionPane.showMessageDialog(null, "To see the list of participants, press 'ok' ");
            JOptionPane.showMessageDialog(null, "Ski Biathlon Standings :  ");


            int place = 0;
            String[] places = {"Winner  ", "Runner-up  ", "Third place  "};

            for (Athlete a: athletes) {
                if (place < 3) {

                    JOptionPane.showMessageDialog(null, "\n " + places[place] +
                            "\n " + "ID: " + a.getAthleteNumber() +
                            "\n " + "Name: " + a.getAthleteName() +
                            "\n " + "Nationality: " + a.getCountryCode()  +
                            "\n " + "Time result : " + a.getSkiTimeResult()  +
                            "\n " + "Penalty time: " + a.getPenaltyTime() + " seconds" +
                            "\n " + "Time after penalty: " + a.convertTime());

                }
                place += 1;
            }



        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                assert reader != null;
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        JOptionPane.showMessageDialog(null, "Thanks for watching!  ");
    }
}






