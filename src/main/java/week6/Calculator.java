package week6;

import java.util.ArrayList;
import java.util.List;

    public class Calculator {

        protected String unit;

        public Calculator(String unit) {
            this.unit = unit;
        }

        public double calculate(String calculate) {
            List<String> addList = new ArrayList<>();
            List<String> minusList = new ArrayList<>();
            int checkPoint = 0;
            boolean operation = true;


            for (int i = 1; i < calculate.length(); i++) {
                String u = calculate.substring(i, i + 1);
                if (Operator.PLUS.getOperator().equals(u)) {
                    checkOperator(addList, minusList, operation, calculate.substring(checkPoint, i).trim());
                    checkPoint = i + 1;
                    operation = true;
                    continue;
                }
                if (Operator.MINUS.getOperator().equals(u)) {
                    checkOperator(addList, minusList, operation, calculate.substring(checkPoint, i).trim());
                    checkPoint = i + 1;
                    operation = false;
                }
            }

            checkOperator(addList, minusList, operation, calculate.substring(checkPoint).trim());

            int sumAdd = sumList(addList);
            int sumMinus = sumList(minusList);

            int sum = sumAdd - sumMinus;
            String u = getUnit();
            double unit = Converter.valueOf(u).getUnit();
            return sum / unit;
        }

        private static int sumList(List<String> addList) {
            int sum = 0;
            for (String u: addList) {
                String[] arr = u.split(" ");
                int value = Integer.parseInt(arr[0]);
                int unit = Converter.valueOf(arr[1]).getUnit();
                sum += value * unit;
            }
            return sum;
        }
        private static void checkOperator(List<String> addList, List<String> minusList, boolean operation, String substring) {
            if (operation) {
                addList.add(substring);
            } else {
                minusList.add(substring);
            }
        }

        public String getUnit() {
            return unit;
        }
    }

