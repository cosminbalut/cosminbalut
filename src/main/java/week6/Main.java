package week6;
import javax.swing.*;
public class Main {
    public static void main(String[] args) {

        JOptionPane.showMessageDialog(null, "This is my application.\n It is able to add or subtract the added values simultaneously \n in a selected unit of measurement.\n Enjoy!");

        String expression = JOptionPane.showInputDialog("Please, enter first number, a space, unit (mm, cm , dm , m , km) and operation. \n Example of expression to be input: 10 cm + 1 m - 10 mm ");
        String unit = JOptionPane.showInputDialog("You can choose the output unit system from: mm, cm, dm, m, km");

        Calculator calculator = new Calculator(unit);
        JOptionPane.showMessageDialog(null, "The result for " + expression + " is: " + "\n" + calculator.calculate(expression) + unit + "\n");
        JOptionPane.showMessageDialog(null,"Thanks for using my application.\n Have a nice day!");
    }
}




