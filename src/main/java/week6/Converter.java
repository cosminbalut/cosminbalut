package week6;

public enum Converter  {

    mm(1),
    cm(10),
    dm(100),
    m(1000),
    km(1000000);

    protected int unit;

    Converter(int unit) {
        this.unit = unit;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }
}









