package week11.domain;

public class FestivalAttendeeThread extends Thread {

    private TicketType ticketType;
    private FestivalGate gate;

    public FestivalAttendeeThread(TicketType ticketType, FestivalGate festivalGate) {
        this.ticketType = ticketType;
        this.gate = festivalGate;
    }

    @Override
    public void run() {
        try {
          Thread.sleep(500);

        } catch (Exception e) {
            e.printStackTrace();
        }
        this.gate.getAttendees().add(ticketType);
        System.out.println();
        System.out.println("Through this gate has passed ticket " + ticketType.name());
    }
}
