package week11.domain;


import java.util.EnumMap;
import java.util.Set;


public class FestivalStatisticsThread extends Thread {
    private FestivalGate gate;

    public FestivalStatisticsThread(FestivalGate gate){
        this.gate = gate;
    }

    @Override
    public void run() {
        boolean forFestival = false;
        while (true){
            var attendees = this.gate.getAttendees();

            if (attendees.isEmpty()){
                continue;
            }
            if (forFestival){
                break;
            }
            if (attendees.size() == 200){
                forFestival = true;
            }

            EnumMap<TicketType,Integer> enumMap = new EnumMap<>(TicketType.class);
            for (TicketType ticketType : attendees){
                if (enumMap.containsKey(ticketType)){
                    enumMap.put(ticketType, enumMap.get(ticketType)+1);
                }else {
                    enumMap.put(ticketType, 1);
                }
            }
           Set<TicketType> ticketTypes = enumMap.keySet();
            System.out.println();
            System.out.println("Statistics for festival");
            System.out.println();
            System.out.println(enumMap.values()
                    .stream()
                    .mapToInt(elem -> elem).sum() + " people have entered ");
            for (TicketType t : ticketTypes){
                System.out.println(enumMap.get(t) + " people have " + " ticket type");

            }

            try {
                Thread.sleep(5000);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
