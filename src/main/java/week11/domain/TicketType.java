package week11.domain;

import java.util.Random;

public enum TicketType {
    full, fullVIP,freePass,oneDay, oneDayVIP;

    public static TicketType ticketIsRandom (){
        TicketType[] values = TicketType.values();
        int length = values.length;
        int randomPeople = new Random().nextInt(length);
        return values[randomPeople];
    }
}
