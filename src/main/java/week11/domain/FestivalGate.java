package week11.domain;

import lombok.Data;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

@Data
public class FestivalGate {

    private Queue<TicketType> attendees = new ConcurrentLinkedDeque<>();

}
