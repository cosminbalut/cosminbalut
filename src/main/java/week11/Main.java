package week11;

import week11.domain.FestivalAttendeeThread;
import week11.domain.FestivalGate;
import week11.domain.FestivalStatisticsThread;
import week11.domain.TicketType;

public class Main {
    public static void main(String args[]){

        FestivalGate gate = new FestivalGate();
        FestivalStatisticsThread statisticsThread = new FestivalStatisticsThread(gate);
        statisticsThread.start();

        for (int i = 0; i < 200; i++){
            TicketType ticketType = TicketType.ticketIsRandom();
            FestivalAttendeeThread festivalAttendeeThread = new FestivalAttendeeThread(ticketType,gate);
            Thread t = new Thread(festivalAttendeeThread);
            t.start();


            try {
               Thread.sleep(100);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


