package test.testCSVweek8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import week8.domain.Athlete;

import static org.junit.jupiter.api.Assertions.*;

class AthleteTest {
    Athlete athlete;

    @BeforeEach
    void setup() {
        athlete = new Athlete();
        athlete.setAthleteNumber(11);
        athlete.setAthleteName("James Sulivan");
        athlete.setCountryCode(Athlete.CountryCode.UK);
        athlete.setSkiTimeResult("22:10");
        athlete.setFirstShootingRange("xxxoo");
        athlete.setSecondShootingRange("xxxxo");
        athlete.setThirdShootingRange("xxxxx");

    }

    @Test
    void getAthleteNumber_success() {
        assertEquals(11, athlete.getAthleteNumber(), "Should return only the number");
        System.out.println("This test has passed successfully! " );

    }

    @Test
    void getAthleteName_success() {
        assertEquals("James Sulivan", athlete.getAthleteName(), "Should return only the name");
        System.out.println("This test has passed successfully! " );
    }


    @Test
    void getSkiTimeResult_success() {
        assertEquals("22:10", athlete.getSkiTimeResult(), "Should return only the ski time result");
        System.out.println("This test has passed successfully! " );

    }
}