package test.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import week6.Calculator;


class CalculatorTest {

    private Calculator calculator;
    private String unit;
    private String expression;

    @BeforeEach
    void SetUp() {
        unit = "mm";
        calculator = new Calculator(unit);
        expression = " 10 cm + 1 m - 10 mm";

    }

    @Test
    void calculateExpressionToMM_success() {
        double actualResult = calculator.calculate(expression);
        if (actualResult == 1090) {
            System.out.println("This test has passed successfully! The result is: " + actualResult + " " + unit);

        } else {
            System.err.println("This test has failed! The result is: " + actualResult + " " + unit);
        }
    }

    @Test
    void calculateExpressionToMM_fail() {
        double actualResult = calculator.calculate(expression);
        if (actualResult < 1090) {
            System.out.println("This test has passed successfully! The result is: " + actualResult + " " + unit);

        } else {
            System.err.println("This test has failed! The result is: " + actualResult + " " + unit);
        }
    }


    @Test
    void convertToM_success() {
        String unit = "m";
        calculator = new Calculator(unit);
        double actualResult = calculator.calculate(expression);
        if (actualResult == 1.09) {
            System.out.println("This test has passed successfully! The result is: " + actualResult + " " + unit);
        } else {
            System.out.println("This test has failed! The result is: " + actualResult + " " + unit);
        }

    }
}