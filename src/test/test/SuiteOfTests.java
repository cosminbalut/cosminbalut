package test.test;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("My test <Băluț Cosmin>")
@SelectClasses(CalculatorTest.class)
public class SuiteOfTests {
}
