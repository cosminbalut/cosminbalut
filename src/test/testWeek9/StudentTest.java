package test.testWeek9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import week9.domain.Gender;
import week9.domain.MyException;
import week9.domain.Student;

import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;


class StudentTest {

        @Test
        @DisplayName("I created a person without an first name and display the exception from MyException class with success")
        void createStudent_firstNameIsEmpty_successWithMyException(){
                Exception exception = assertThrows(MyException.class, () -> new Student(null, "Morata", LocalDate.of(1894, 1, 1), Gender.M, "1234567890123"));
                Assertions.assertEquals("First name should not be empty!",exception.getMessage());
        }

        @Test
        @DisplayName("I created a person without an last name")
        void createStudent_lastNameIsEmpty_successWithMyException(){
                Exception exception = assertThrows(MyException.class, () -> new Student("Alvaro", null, LocalDate.of(1894, 1, 1), Gender.M, "1234567890123"));
                Assertions.assertEquals("Last name should not be empty!",exception.getMessage());
        }

        @Test
        @DisplayName("I created a person who was born before years 1900")
        void createStudent_bornBefore1900_successWithMyException() {
                Exception exception = assertThrows(MyException.class, () -> new Student("Davis", "McLean", LocalDate.of(1890, 12, 13), Gender.M, "1890123987452"));
                assertEquals("Error! You are too old! Are you sure you're still alive?", exception.getMessage());
        }

        @Test
        @DisplayName("I created a person who was born after years 1900")
        void createStudent_under18Year_successWithMyException(){
                Exception exception = assertThrows(MyException.class, () -> new Student("Karim", "Benzema", LocalDate.of(2010,12,19),Gender.M,"1875436890134"));
                assertEquals("The minimum age is 18 years. thanks for understanding!", exception.getMessage());
        }

        @Test
        @DisplayName("This student has a cnp of 11 characters")
        void createStudent_wrongCNP_successWithMyException(){
                Exception exception = assertThrows(MyException.class, () -> new Student("Bianca", "Alexe",LocalDate.of(1998,8,17) ,Gender.F ,"19800065753" ));
                assertEquals("Wrong CNP. Please try again", exception.getMessage());
        }

        @Test
        @DisplayName("This student has null cnp")
        void createStudent_nullCNP_successWithMyException(){
                Exception exception = assertThrows(MyException.class, () -> new Student("Ana", "Enache",LocalDate.of(1994,4,24) ,Gender.F ,null ));
                assertEquals("Please fill in the blanks, then try again", exception.getMessage());
        }
    }
