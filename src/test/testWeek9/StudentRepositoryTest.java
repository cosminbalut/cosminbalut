package test.testWeek9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import week9.domain.Gender;
import week9.domain.MyException;
import week9.domain.Student;
import week9.domain.StudentRepository;

import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

class StudentRepositoryTest {

    StudentRepository studentRepository;
    Student student ;

    @BeforeEach
    public void setUp() {
        studentRepository = new StudentRepository();
        student = new Student("Florin", "Lazar", LocalDate.of(1974, 5, 19), Gender.M, "1743789670011");
    }

    @Test
    @DisplayName("Add a student")

    void addStudent_success() {
        studentRepository.addStudent(student);
        assertEquals(1, studentRepository.students.size());
    }

    @Test
    @DisplayName("Remove student by CNP")

    void deleteUsingCNP_success() {
        studentRepository.addStudent(student);
        studentRepository.deleteUsingCNP("1743789670011");
        assertEquals(0,studentRepository.students.size());
    }

    @Test
    @DisplayName("This student has been removed from the list because the CNP is wrong")

    void deleteUsingCNP_wrongCNP_successWithMyException() {
        studentRepository.addStudent(student);
        Exception exception = assertThrows(MyException.class, () -> studentRepository.deleteUsingCNP("0000"));
        assertEquals("Please add a valid cnp!",exception.getMessage());
    }

    @Test
    @DisplayName("This student has been removed from the list because the CNP is null")

    void deleteUsingCNP_nullCNP_successWithMyException() {
        studentRepository.addStudent(student);
        Exception exception = assertThrows(MyException.class, () -> studentRepository.deleteUsingCNP(null));
        assertEquals("Please fill in the blanks!",exception.getMessage());
    }

    @Test
    @DisplayName("This CNP does not exist")
    void deleteUsingCNP_cnpDoesNotExist_successWithMyException() {
        studentRepository.addStudent(student);
        Exception exception = assertThrows(MyException.class, () -> studentRepository.deleteUsingCNP("0074378970011"));
        assertEquals("This CNP does not exist",exception.getMessage());
    }

    @Test
    @DisplayName("For this test the student age is real")
    void retrieveStudents_success(){
        studentRepository.retrieveStudents("76");
        assertEquals(76, 76);
    }

    @Test
    @DisplayName("For this test the student's age was negative")

    void retrieveStudents_ageIsNegative_successWithMyException() {
        Exception exception = assertThrows(MyException.class,() -> studentRepository.retrieveStudents("-25"));
        assertEquals("Age is negative!", exception.getMessage());
    }

    @Test
    @DisplayName("For this test the student's age isn't a number ")

    void retrieveStudents_ageIsNotNumber_successWithMyException() {
        Exception exception = assertThrows(MyException.class,() -> studentRepository.retrieveStudents("abc"));
        assertEquals("Age isn't a number!", exception.getMessage());
    }

    @Test
    @DisplayName("For this list I added 3 students")

    void listStudents_success() {
        studentRepository.addStudent(student);
        studentRepository.addStudent(student);
        studentRepository.addStudent(student);
        assertEquals(3, studentRepository.students.size());

    }
}