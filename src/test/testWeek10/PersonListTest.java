package week10.domain;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonListTest {

    PersonList person;

    @BeforeEach
    void setup() {
        person = new PersonList("Andrei","Dumitrache","1996-09-23");
    }

    @Test
    void createPerson_getFirstName_success(){
        Assertions.assertEquals("Andrei",person.getFirstName());
        System.out.println("Passed! The expected first name and actual first name are the same.");
    }

    @Test
    void createPerson_getFirstName_fail(){
        Assertions.assertNotEquals("Vlad",person.getFirstName());
        System.out.println("Passed! The expected first name and actual first name are not the same.");
    }


    @Test
    void createPerson_getLastName_success() {
        Assertions.assertEquals("Dumitrache",person.getLastName());
        System.out.println("Passed! The expected last name and actual last name are the same.");
    }

    @Test
    void createPerson_getLastName_fail() {
        Assertions.assertNotEquals("Albu",person.getLastName());
        System.out.println("Passed! The expected last name and actual last name are not the same.");
    }

    @Test
    void createPerson_getDateOfBirth_fail() {
        Assertions.assertNotEquals("1996-01-13", person.getDateOfBirth());
        System.out.println("Passed! The expected date of birth and actual date of birth are not the same.");
    }
}